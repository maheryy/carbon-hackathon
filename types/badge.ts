import { Prisma } from "@prisma/client";

export type FullUserBadge = Prisma.UserBadgeGetPayload<{
  include: {
    badge: true;
  };
}>;
