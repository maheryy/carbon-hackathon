import { Prisma } from "@prisma/client";

export type UserWithBadges = Prisma.UserGetPayload<{
  include: { userBadges: { include: { badge: true } } };
}>;
