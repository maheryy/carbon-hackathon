import {
  Client,
  Experience,
  Mission,
  Position,
  PrismaClient,
  User,
} from "@prisma/client";
import bcrypt from "bcryptjs";
import { faker } from "@faker-js/faker";

const prisma = new PrismaClient();

const skills = [
  "Typescript",
  "React.js",
  "Vue.js",
  "Node.js",
  "Next.js",
  "Symfony",
  "Java",
  ".NET",
];

const password = bcrypt.hashSync("password", 10);
const users = [
  {
    email: "rh@carbon.com",
    experience: Experience.EXPERT,
    firstname: "Emily",
    lastname: "Barnes",
    position: Position.HR,
    address: faker.location.streetAddress({ useFullAddress: true }),
    password: password,
    contractStart: faker.date.past(),
    contractEnd: faker.date.future(),
    birthday: faker.date.past({ years: 18 }),
    picture: faker.image.avatar(),
  },
  {
    email: "sales@carbon.com",
    experience: Experience.EXPERT,
    firstname: "Billy",
    lastname: "Bones",
    position: Position.SALES,
    address: faker.location.streetAddress({ useFullAddress: true }),
    password: password,
    contractStart: faker.date.past(),
    contractEnd: faker.date.future(),
    birthday: faker.date.past({ years: 18 }),
    picture: faker.image.avatar(),
  },
  {
    email: "dev@carbon.com",
    experience: Experience.JUNIOR,
    firstname: "Paul",
    lastname: "Harrison",
    position: Position.DEVELOPER,
    address: faker.location.streetAddress({ useFullAddress: true }),
    password: password,
    contractStart: faker.date.past(),
    contractEnd: faker.date.future(),
    birthday: faker.date.past({ years: 18 }),
    skills: faker.helpers.arrayElements(skills, {
      min: 1,
      max: 5,
    }),
    picture: faker.image.avatar(),
  },
  ...Array.from({ length: 7 }).map((_, i) => ({
    email: faker.internet.email(),
    experience: faker.helpers.arrayElement(Object.values(Experience)),
    password: password,
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    position: Position.DEVELOPER,
    address: faker.location.streetAddress({ useFullAddress: true }),
    skills: faker.helpers.arrayElements(skills, {
      min: 1,
      max: 5,
    }),
    contractStart: faker.date.past(),
    contractEnd: faker.date.future(),
    birthday: faker.date.past({ years: 18 }),
    picture: faker.image.avatar(),
  })),
];

const clients = [
  ...Array.from({ length: 10 }).map((_, i) => ({
    company: faker.company.name(),
    email: faker.internet.email(),
    name: faker.person.fullName(),
    phone: faker.phone.number(),
    position: faker.commerce.department(),
  })),
];

const badges = [
  { path: "event/conferencier.png" },
  { path: "event/evenementiel.png" },
  { path: "event/organisateur.png" },
  { path: "lead/inspirant.png" },
  { path: "lead/mentor.png" },
  { path: "lead/opinion.png" },
  { path: "performance/collaborateur.png" },
  { path: "performance/leader.png" },
  { path: "performance/proactif.png" },
  { path: "training/expert.png" },
  { path: "training/novice.png" },
  { path: "training/polyvalent.png" },
];

const events = (organizerId: number) => {
  return Array.from({ length: 10 }).map((_, i) => ({
    title: faker.word.words(3),
    date: faker.date.future(),
    description: faker.lorem.paragraph(2),
    organizerId: organizerId,
  }));
};

const training = (authorId: number) => {
  return Array.from({ length: 10 }).map((_, i) => ({
    title: faker.word.words(3),
    description: faker.lorem.paragraph(2),
    authorId: authorId,
    adress: faker.location.streetAddress({ useFullAddress: true }),
    startDate: faker.date.future(),
    endDate: faker.date.future(),
  }));
};

const missions = (users: User[], clients: Client[]) => {
  return Array.from({ length: 10 }).map((_, i) => ({
    startDate: faker.date.past(),
    title: faker.word.words(3),
    description: faker.lorem.paragraph(2),
    consultantId: faker.helpers.arrayElement(users).id,
    clientId: faker.helpers.arrayElement(clients).id,
  }));
};

const feedbacks = (missions: Mission[]) => {
  return missions.map((mission, i) => ({
    context: faker.lorem.paragraph(2),
    comments: faker.lorem.paragraph(2),
    improvements: faker.lorem.paragraph(2),
    positives: faker.lorem.paragraph(2),
    rating: faker.helpers.rangeToNumber({ min: 1, max: 5 }),
    consultantId: mission.consultantId,
    clientId: mission.clientId,
    missionId: mission.id,
    date: faker.date.past(),
  }));
};

async function main() {
  await prisma.$transaction([
    prisma.userBadge.deleteMany(),
    prisma.userEvent.deleteMany(),
    prisma.userTraining.deleteMany(),
    prisma.feedback.deleteMany(),
    prisma.mission.deleteMany(),
    prisma.client.deleteMany(),
    prisma.badge.deleteMany(),
    prisma.event.deleteMany(),
    prisma.training.deleteMany(),
    prisma.user.deleteMany(),
  ]);

  await prisma.user.createMany({ data: users });
  await prisma.badge.createMany({ data: badges });
  await prisma.client.createMany({ data: clients });

  const _clients = await prisma.client.findMany();
  const _badges = await prisma.badge.findMany();

  const _rh = await prisma.user.findFirst({ where: { position: "HR" } });
  const _devs = await prisma.user.findMany({
    where: { position: "DEVELOPER" },
  });

  await prisma.mission.createMany({ data: missions(_devs, _clients) });
  await prisma.training.createMany({ data: training(_rh.id) });
  await prisma.event.createMany({ data: events(_rh.id) });

  const _missions = await prisma.mission.findMany();
  const _trainings = await prisma.training.findMany();
  const _events = await prisma.event.findMany();
  await prisma.feedback.createMany({ data: feedbacks(_missions) });

  for (const dev of _devs) {
    await prisma.user.update({
      where: {
        id: dev.id,
      },
      data: {
        userBadges: {
          create: [
            ...faker.helpers
              .arrayElements(_badges, { min: 1, max: 5 })
              .map((badge) => ({ badgeId: badge.id, date: faker.date.past() })),
          ],
        },
        userEvent: {
          create: [
            ...faker.helpers
              .arrayElements(_events, { min: 1, max: 5 })
              .map((event) => ({ eventId: event.id })),
          ],
        },
        userTrainings: {
          create: [
            ...faker.helpers
              .arrayElements(_trainings, { min: 1, max: 5 })
              .map((training) => ({
                trainingId: training.id,
                startDate: faker.date.past(),
              })),
          ],
        },
      },
    });
  }
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
