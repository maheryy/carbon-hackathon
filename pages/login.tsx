import type { GetServerSidePropsContext } from "next";
import { getServerSession } from "next-auth";
import { getCsrfToken, getSession, signIn } from "next-auth/react";
import { useRouter } from "next/router";
import { useState } from "react";
import { authOptions } from "./api/auth/[...nextauth]";

export const getServerSideProps = async (
  context: GetServerSidePropsContext
) => {
  const session = await getServerSession(context.req, context.res, authOptions);
  if (session) {
    return {
      redirect: { destination: "/" },
    };
  }

  return {
    props: {},
  };
};

const Login = () => {
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const onSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    try {
      setLoading(true);

      const res = await signIn("credentials", {
        email: email,
        password: password,
        redirect: false,
        callbackUrl: "/",
      });

      if (res.error) {
        throw new Error("Votre email ou mot de passe est incorrect");
      }

      setLoading(false);
      router.push("/");
    } catch (error) {
      setLoading(false);
      setError((error as Error).message);
    }
  };

  return (
    <main className="min-h-screen bg-carbonBlack flex justify-center items-center">
      <div className="w-1/3 flex flex-col items-center justify-center gap-10">
        <span>
          <img src="/assets/Logo.svg" className="mb-10 w-96" />
        </span>
        <div className="w-full bg-white rounded-[30px] p-16">
          {error && (
            <p className="relative -top-5 text-center bg-red-300 py-4 mb-6 rounded-xl">
              {error}
            </p>
          )}
          <form onSubmit={onSubmit}>
            <div className="flex flex-col gap-6 items-start">
              <div className="flex flex-col gap-2 items-start w-full">
                <label htmlFor="email" className="text-[#7F7E83] text-lg">
                  Email
                </label>
                <input
                  id="email"
                  name="email"
                  type="email"
                  value={email}
                  className="w-full bg-[#F4F4F4] p-3 rounded-md"
                  onChange={(e) => setEmail(e.target.value)}
                  required
                />
              </div>
              <div className="flex flex-col gap-2 items-start w-full">
                <label htmlFor="password" className="text-[#7F7E83] text-lg">
                  Mot de passe
                </label>
                <input
                  id="password"
                  name="password"
                  type="password"
                  value={password}
                  className="w-full bg-[#F4F4F4] p-3 rounded-md"
                  onChange={(e) => setPassword(e.target.value)}
                  required
                />
              </div>
              <button
                type="submit"
                disabled={loading}
                className="bg-[#6996CD] text-white rounded-md py-2 px-8 self-center mt-4 disabled:bg-[#8ea9cb] disabled:cursor-progress"
              >
                Connexion
              </button>
            </div>
          </form>
        </div>
      </div>
    </main>
  );
};

Login.displayName = "Login";

export default Login;
