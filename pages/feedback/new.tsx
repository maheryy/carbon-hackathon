import { User } from "@prisma/client";
import Navbar from "components/navbar";
import { useRouter } from "next/router";
import { ChangeEvent, FormEvent, useState } from "react";
import { NextPageContext } from "next";
import RatingStars from 'react-rating-stars-component';
import { getAllClientsConsultant } from "services/mission";

export const getServerSideProps = async (context: NextPageContext) => {
  const id = parseInt(context.query.id as string);
  const consultantPerClient = await getAllClientsConsultant(3);

  return { props: { consultantPerClient } };
};

const NewFeedback = (consultantPerClient: any) => {
  const [context, setContext] = useState("");
  const [positif, setPositif] = useState("");
  const [improvement, setImprovement] = useState("");
  const [comment, setComment] = useState("");
  const [date, setDate] = useState(new Date());
  const [rating, setRating] = useState(0);
  const [selectedConsultant, setselectedConsultant] = useState(0);
  const router = useRouter();

  const handleRatingChange = (newRating: number) => {
    setRating(newRating);
  };

  const handleConsultantChange = (event: ChangeEvent<HTMLSelectElement>) => {
    setselectedConsultant(parseInt(event.target.value, 10));
  };

  const handleCancel = () => {
    router.back();
  };

  const onSubmit = async (e: FormEvent) => {
    e.preventDefault();
    const formData = {
      context: context,
      positives: positif,
      improvements: improvement,
      comments: comment,
      rating: rating,
      date: date,
      missionId: consultantPerClient.consultantPerClient[0].id,
      consultantId: selectedConsultant,
      clientId: consultantPerClient.consultantPerClient[0].client.id,
    };

    try {
      const res = await fetch("/api/feedback", {
        method: "POST",
        body: JSON.stringify(formData),
        headers: { "Content-Type": "application/json" },
      });

      if (res.ok) {
        const feedback = await res.json();
        router.push(`/feedback`);
      } else {
        console.log("Une erreur s'est produite lors de la création du feedback.");
      }
    } catch (error) {
      console.log("Une erreur s'est produite lors de la création du feedback:", error);
    }
  };

  return (
    <>
      <Navbar/>
      <div className="flex flex-col items-center justify-center min-h-screen py-2">
        <div className="w-full max-w-md p-1">
          <div className="w-full h-4/5 place-content-center bg-white shadow-xl p-8 rounded">
            <p className="font-bold text-2xl text-center mb-6">
              Faites un retour de notre consultant :
            </p>
            <form onSubmit={onSubmit}>
              <div className="mb-6">
                <label htmlFor="email" className="block mb-2 text-sm font-normal text-inputLabelColor">Consultant concerné</label>
                {consultantPerClient.consultantPerClient.map((consultant, indexConsultant) => (
                  <div key={indexConsultant}>
                  <select 
                    name="notation" 
                    id="notation-select" 
                    className="bg-inputColor rounded-lg w-96 h-12" 
                    value={selectedConsultant} 
                    onChange={handleConsultantChange}
                  >
                    <option className="text-gray-500" value="">Choisissez le consultant</option>
                    <option 
                      value={consultant.consultant.id} 
                      key={indexConsultant}
                    >
                      {consultant.consultant.firstname} {consultant.consultant.lastname}
                    </option>
                  </select>
                </div>
                ))}
              </div>
              <div className="mb-6">
                <label htmlFor="email" className="block mb-2 text-sm font-normal text-inputLabelColor">Context de la mission</label>
                <input
                  type="text"
                  id="context"
                  name="context"
                  className="bg-inputColor rounded-lg w-96 h-12 dark:placeholder-inputPlaceholderColor"
                  placeholder="Le but de la mission était de ..."
                  value={context}
                  onChange={(e) => setContext(e.target.value)}
                  required
                />
              </div>
              <div className="mb-6">
                <label htmlFor="text" className="block mb-2 text-sm font-normal text-inputLabelColor">Points positifs</label>
                <input
                  type="text"
                  id="positifs"
                  name="positifs"
                  className="bg-inputColor rounded-lg w-96 h-12 dark:placeholder-inputPlaceholderColor"
                  placeholder="Les points positifs sont ..."
                  value={positif}
                  onChange={(e) => setPositif(e.target.value)}
                  required
                />
              </div>
              <div className="mb-6">
                <label htmlFor="text" className="block mb-2 text-sm font-normal text-inputLabelColor">Points à améliorer</label>
                <input
                  type="text"
                  id="improvements"
                  name="improvements"
                  className="bg-inputColor rounded-lg w-96 h-12 dark:placeholder-inputPlaceholderColor"
                  placeholder="Les points à améliorer sont ..."
                  value={improvement}
                  onChange={(e) => setImprovement(e.target.value)}
                  required
                />
              </div>
              <div className="mb-6">
                <label htmlFor="text" className="block mb-2 text-sm font-normal text-inputLabelColor">Commentaire</label>
                <input
                  type="text"
                  id="comment"
                  name="comment"
                  className="bg-inputColor rounded-lg w-96 h-12 dark:placeholder-inputPlaceholderColor"
                  placeholder="Le consultant est ..."
                  value={comment}
                  onChange={(e) => setComment(e.target.value)}
                  required
                />
              </div>
              <div className="mb-6">
                <label htmlFor="text" className="block mb-2 text-sm font-normal text-inputLabelColor">Notation</label>
                <RatingStars
                  count={5}
                  value={rating}
                  size={24}
                  activeColor="#FFC107"
                  edit={true}
                  onChange={handleRatingChange}
                />
              </div>
              <div className="grid gap-4 grid-cols-2">
                <button className="text-white bg-colorButton w-36 h-7 rounded-lg" onClick={handleCancel}>
                  Annuler
                </button>
                <button type="submit" className="text-white bg-colorButton w-36 h-7 rounded-lg">Envoyer</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default NewFeedback;
