import ConsultantCard from "components/card/ConsultantCard";
import Link from "next/link";
import { NextPageContext } from "next";
import { getAllFeedback, getAllFeedbackByClient } from "services/feedback";

export const getServerSideProps = async (context: NextPageContext) => {
  const id = parseInt(context.query.id as string);
  const feedbackList = await getAllFeedback();
  const getFeedbackByClient = await getAllFeedbackByClient(1);

  return { props: { feedbackList, getFeedbackByClient } };
};

const FeedbackList = (feedbackList: any) => {
  console.log(feedbackList)
  return (
    <div>
      <div className="flex flex-col items-center justify-center min-h-screen py-2">
        <p className="font-bold text-2xl text-center mb-6">
          Les retours clients pour chaque consultants
        </p>
        {feedbackList.getFeedbackByClient[0].clientId === 1 && (
          <>
            <a href="/feedback/new">
              <button className="bg-colorButton text-white font-bold py-2 px-4 m-10 rounded-lg">
                Ajouter un commentaire
              </button>
            </a>
          </>
        )}
        <div className="flex flex-wrap justify-start gap-3 m-5">
          {feedbackList.getFeedbackByClient[0].clientId === 1 ? (
            <>
              {feedbackList.getFeedbackByClient.map((feedback) => (
                <ConsultantCard key={feedback.id} user={feedback.consultant} feedback={feedback.id} />
              ))}
            </>
          ) : (
            <>
              {feedbackList.feedbackList.map((feedback) => (
                <ConsultantCard key={feedback.id} user={feedback.consultant} feedback={feedback.id} />
              ))}
            </>
          )}
        </div>
      </div>
    </div>
  );
};
export default FeedbackList;
