import { Feedback } from "@prisma/client";
import { useRouter } from "next/router";
import { ChangeEvent, FormEvent, useState } from "react";
import { NextPageContext } from "next";
import { getFeedback } from "services/feedback";
import RatingStars from 'react-rating-stars-component';

export const getServerSideProps = async (context: NextPageContext) => {
  const id = parseInt(context.query.id as string);
  const feedback = await getFeedback(id);

  return { props: { feedback } };
};

const EditFeedback = ({ feedback }: EditFeedbackProps) => {
  const [context, setContext] = useState(feedback.context);
  const [positif, setPositif] = useState(feedback.positives);
  const [improvement, setImprovement] = useState(feedback.improvements);
  const [comment, setComment] = useState(feedback.comments);
  const [rating, setRating] = useState(feedback.rating);
  const router = useRouter();

  const handleRatingChange = (newRating: number) => {
    setRating(newRating);
  };

  const handleCancel = () => {
    router.back();
  };

  const onSubmit = async (e: FormEvent) => {
    e.preventDefault();

    const formUpdateData = {
      context: context,
      positives: positif,
      improvements: improvement,
      comments: comment,
      rating: rating,
      missionId: feedback.missionId,
      consultantId: feedback.consultantId,
      clientId: feedback.consultantId
    };

    const res = await fetch(`/api/feedback/${feedback.id}`, {
      method: "PUT",
      body: JSON.stringify(formUpdateData),
      headers: { "Content-Type": "application/json" },
    });

    if (res.status === 200) {
      router.push("/feedback");
    }
  };

  return (
    <>
      <div className="flex flex-col items-center justify-center min-h-screen py-2">
        <div className="w-full max-w-md p-1">
          <div className="w-full h-4/5 place-content-center bg-white shadow-xl p-8 rounded">
            <p className="font-bold text-2xl text-center mb-6">
              Modification du commentaire
            </p>
            <form onSubmit={onSubmit}>
              <div className="mb-4">
                <h2 className="text-lg italic font-semibold text-carbonBlack">Consultant : {feedback.consultant.firstname} {feedback.consultant.lastname}</h2>
              </div>
              <div className="mb-6">
                <label htmlFor="email" className="block mb-2 text-sm font-normal text-inputLabelColor">Context de la mission</label>
                <input
                  type="text"
                  id="context"
                  name="context"
                  className="bg-inputColor rounded-lg w-96 h-12 dark:placeholder-inputPlaceholderColor"
                  placeholder="Le but de la mission était de ..."
                  value={context}
                  onChange={(e) => setContext(e.target.value)}
                  required
                />
              </div>
              <div className="mb-6">
                <label htmlFor="text" className="block mb-2 text-sm font-normal text-inputLabelColor">Points positifs</label>
                <input
                  type="text"
                  id="positifs"
                  name="positifs"
                  className="bg-inputColor rounded-lg w-96 h-12 dark:placeholder-inputPlaceholderColor"
                  placeholder="Les points positifs sont ..."
                  value={positif}
                  onChange={(e) => setPositif(e.target.value)}
                  required
                />
              </div>
              <div className="mb-6">
                <label htmlFor="text" className="block mb-2 text-sm font-normal text-inputLabelColor">Points à améliorer</label>
                <input
                  type="text"
                  id="improvements"
                  name="improvements"
                  className="bg-inputColor rounded-lg w-96 h-12 dark:placeholder-inputPlaceholderColor"
                  placeholder="Les points à améliorer sont ..."
                  value={improvement}
                  onChange={(e) => setImprovement(e.target.value)}
                  required
                />
              </div>
              <div className="mb-6">
                <label htmlFor="text" className="block mb-2 text-sm font-normal text-inputLabelColor">Commentaire</label>
                <input
                  type="text"
                  id="comment"
                  name="comment"
                  className="bg-inputColor rounded-lg w-96 h-12 dark:placeholder-inputPlaceholderColor"
                  placeholder="Le consultant est ..."
                  value={comment}
                  onChange={(e) => setComment(e.target.value)}
                  required
                />
              </div>
              <div className="mb-6">
                <label htmlFor="text" className="block mb-2 text-sm font-normal text-inputLabelColor">Notation</label>
                <RatingStars
                  count={5}
                  value={rating}
                  size={24}
                  activeColor="#FFC107"
                  edit={true}
                  onChange={handleRatingChange}
                />
              </div>
              <div className="grid gap-4 grid-cols-2">
                <button className="text-white bg-colorButton w-36 h-7 rounded-lg" onClick={handleCancel}>Annuler</button>
                <button type="submit" className="text-white bg-colorButton w-36 h-7 rounded-lg">Enregistrer</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  )
};

interface EditFeedbackProps {
  feedback: Feedback;
}

export default EditFeedback;