import { Todo } from "@prisma/client";
import Link from "next/link";
import { getFeedback, getAllFeedbackByClient } from "services/feedback";
import { NextPageContext } from "next";
import RatingStars from 'react-rating-stars-component';

export const getServerSideProps = async (context: NextPageContext) => {
  const id = parseInt(context.query.id as string);
  const feedbackDetail = await getFeedback(id);

  return { props: { feedbackDetail } };
};

const FeedbackDetail = (feedbackDetail: any) => {
  return (
    <>
      <div className="flex flex-col items-center">
        <a href="/feedback" className="text-carbonBlack font-bold mt-2">Retour</a>
        <h1 className="text-2xl font-bold mb-10">Détail du feedback</h1>
        <div className="bg-gray-100 p-4 rounded-lg">
          <div className="text-center mb-10">
            <h2 className="text-xl font-semibold text-carbonBlack">
              Retour fait par {feedbackDetail.feedbackDetail.client.name}
            </h2>
            <p className="text-gray-700">
              {feedbackDetail.feedbackDetail.client.company} - {feedbackDetail.feedbackDetail.client.position}
            </p>
          </div>

          <div className="mb-4">
            <h2 className="text-lg italic font-semibold text-carbonBlack">Consultant : {feedbackDetail.feedbackDetail.consultant.firstname} {feedbackDetail.feedbackDetail.consultant.lastname}</h2>
          </div>
          <div className="mb-4">
            <h2 className="text-lg italic font-semibold text-carbonBlack">Le context de la mission</h2>
            <div className="flex">
              <div className="bg-carbonGreen text-white rounded-lg py-2 px-4 max-w-md">
                <p className="text-sm">{feedbackDetail.feedbackDetail.context}</p>
              </div>
            </div>
          </div>
          <div className="mb-4">
            <h2 className="text-lg italic font-semibold text-carbonBlack">Les points positifs</h2>
            <div className="flex mt-2">
              <div className="bg-carbonGreen text-white rounded-lg py-2 px-4 max-w-md">
                <p className="text-sm">{feedbackDetail.feedbackDetail.positives}</p>
              </div>
            </div>
          </div>
          <div className="mb-4">
            <h2 className="text-lg italic font-semibold text-carbonBlack">Les points à améliorer</h2>
            <div className="flex">
              <div className="bg-carbonGreen text-white rounded-lg py-2 px-4 max-w-md">
                <p className="text-sm">{feedbackDetail.feedbackDetail.improvements}</p>
              </div>
            </div>
          </div>
          <div className="mb-4">
            <h2 className="text-lg italic font-semibold text-carbonBlack">Le commentaire</h2>
            <div className="flex">
              <div className="bg-carbonGreen text-white rounded-lg py-2 px-4 max-w-md">
                <p className="text-sm">{feedbackDetail.feedbackDetail.comments}</p>
              </div>
            </div>
          </div>
          <div className="mb-4">
            <h2 className="text-lg italic font-semibold">La notation</h2>
            <div className="flex">
              <RatingStars
                count={5}
                value={feedbackDetail.feedbackDetail.rating}
                size={24}
                activeColor="#FFC107"
                edit={false}
              />
            </div>
          </div>
        </div>
        {feedbackDetail.feedbackDetail.clientId === 1 && (
          <>
            <button className="bg-colorButton text-white font-bold py-2 px-4 m-10 rounded-lg">
              <a href={`/feedback/${feedbackDetail.feedbackDetail.id}/edit`}>Modifier</a>
            </button>
          </>
        )}
    </div>
    </>
  );
};

export default FeedbackDetail;
