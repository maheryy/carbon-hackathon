import { Event, Training, User } from "@prisma/client";
import ProfileCard from "components/card/ProfileCard";
import ConsultantSection from "components/dashboard/ConsultantSection";
import EventSection from "components/dashboard/EventSection";
import NewsletterHero from "components/dashboard/NewsletterHero";
import TrainingSection from "components/dashboard/TrainingSection";
import { getServerSession } from "next-auth";
import { getAllEvents } from "services/events";
import { getAllTrainings } from "services/training";
import { getAllUsers, getUserByEmail } from "services/user";
import { UserWithBadges } from "types/user";
import { serializeProps } from "utils/parser";
import { authOptions } from "./api/auth/[...nextauth]";

export const getServerSideProps = async (context) => {
  const session = await getServerSession(context.req, context.res, authOptions);

  if (!session) {
    return {
      redirect: { destination: "/login", permanent: false },
    };
  }

  const user = await getUserByEmail(session.user.email);
  const consultants = await getAllUsers();
  const events = await getAllEvents();
  const trainings = await getAllTrainings();

  return {
    props: serializeProps({ user, consultants, events, trainings }),
  };
};

const Dashboard = ({
  user,
  consultants,
  events,
  trainings,
}: DashboardProps) => {
  return (
    <main>
      <div className="p-6 grid grid-cols-3 gap-12">
        <div className="col-span-2 flex flex-col gap-8">
          <div className="relative">
            <input
              type="text"
              placeholder="Recherche"
              className="pl-12 w-2/5 border h-10 p-4 bg-[#F9F9F9] border-[#E8E8EA] rounded-2xl"
            />
            <span className="absolute left-4 top-1/2 -translate-y-1/2">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-5 h-5"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
                />
              </svg>
            </span>
          </div>
          <NewsletterHero />
          <TrainingSection trainings={trainings.slice(0, 3)} />
          <EventSection events={events.slice(0, 3)} />
        </div>
        <div className="col-span-1 flex flex-col gap-6">
          <ProfileCard user={user} />
          <ConsultantSection consultants={consultants.slice(0, 3)} />
        </div>
      </div>
    </main>
  );
};

interface DashboardProps {
  user: UserWithBadges;
  trainings: Training[];
  events: Event[];
  consultants: User[];
}

export default Dashboard;
