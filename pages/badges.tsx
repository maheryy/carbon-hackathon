import { User } from "@prisma/client";
import BadgeSection from "components/badges/BadgeSection";
import ProfileCard from "components/card/ProfileCard";
import ConsultantSection from "components/dashboard/ConsultantSection";
import { getServerSession } from "next-auth";
import { getAllUsers, getUserByEmail } from "services/user";
import { FullUserBadge } from "types/badge";
import { UserWithBadges } from "types/user";
import { serializeProps } from "utils/parser";
import { authOptions } from "./api/auth/[...nextauth]";

export const getServerSideProps = async (context) => {
  const session = await getServerSession(context.req, context.res, authOptions);

  if (!session) {
    return {
      redirect: { destination: "/login", permanent: false },
    };
  }

  const user = await getUserByEmail(session.user.email);
  const consultants = await getAllUsers();

  return {
    props: serializeProps({ user, consultants }),
  };
};

const Badges = ({ user, consultants }: BadgesProps) => {
  const filterBadges = (badges: FullUserBadge[], path: string) =>
    badges.filter((data) => data.badge.path.includes(path));

  return (
    <main className="p-6 grid grid-cols-3 gap-12">
      <div className="flex flex-col gap-10 shadow-md col-span-2 p-14">
        <BadgeSection
          title="Badges formations"
          badges={filterBadges(user.userBadges, "training")}
        />
        <BadgeSection
          title="Badges évènements"
          badges={filterBadges(user.userBadges, "event")}
        />
        <BadgeSection
          title="Badges performances"
          badges={filterBadges(user.userBadges, "performance")}
        />
        <BadgeSection
          title="Badges reconnaissances"
          badges={filterBadges(user.userBadges, "lead")}
        />
      </div>
      <div className="col-span-1 flex flex-col gap-6">
        <ProfileCard user={user} />
        <ConsultantSection consultants={consultants.slice(0, 3)} />
      </div>
    </main>
  );
};

interface BadgesProps {
  user: UserWithBadges;
  consultants: User[];
}

export default Badges;
