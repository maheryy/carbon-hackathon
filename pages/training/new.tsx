import { useRouter } from "next/router";
import { FormEvent, useState } from "react";
import { getUser } from "services/user";

export const getServerSideProps = async () => {
  const user = await getUser(2);
  return { props: { user } };
};

const NewTraining = ({ user }) => {
  const [title, setTitle] = useState("");
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const [adress, setAdress] = useState("");
  const [description, setDescription] = useState("");

  const router = useRouter();

  const onSubmit = async (e: FormEvent,) => {
    e.preventDefault();
    const startDateObj = new Date(startDate);
    const endDateObj = new Date(endDate);
    const res = await fetch("/api/training", {
      method: "POST",
      body: JSON.stringify({
        title,
        startDate: startDateObj,
        endDate: endDateObj,
        adress,
        description,
        authorId: user.id
      }),
      headers: { "Content-Type": "application/json" },
    });

    if (res.status === 201) {
      router.push("/training");
    }
  };

  return (
    <div className="flex flex-col items-center justify-center min-h-screen py-2">
      <div className="w-full max-w-md p-1">
        <h1 className="text-lg">Nouvelle formation</h1>

        <div className="w-150 h-160 place-content-center bg-white shadow-xl p-8 rounded">
          <form className="pt-5" onSubmit={onSubmit}>
            <div className="grid gap-6 mb-6">
              <div>
                <label className="block mb-2 text-sm font-medium text-carbonGray" htmlFor="title">Titre</label>
                <input
                  type="text"
                  className="bg-carbonInputGray text-black text-sm rounded-md w-full p-2.5 dark:placeholder-gray-400 focus:outline-none focus:ring-2 focus:ring-carbonGray"
                  id="title"
                  name="title"
                  value={title}
                  placeholder="Titre"
                  onChange={(e) => setTitle(e.target.value)}
                  required
                />
              </div>

              <div>
                <label className="block mb-2 text-sm font-medium text-carbonGray" htmlFor="description">Description</label>
                <textarea
                  id="description"
                  name="description"
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                  rows="4"
                  className="bg-carbonInputGray text-black text-sm rounded-md w-full p-2.5 dark:placeholder-gray-400 focus:outline-none focus:ring-2 focus:ring-carbonGray"
                  placeholder="Ecrivez votre description ici...">
                </textarea>
              </div>

              <div>
                <label htmlFor="startDate" className="block mb-2 text-sm font-medium text-carbonGray">Date de début de formation</label>
                <input
                  type="date"
                  className="bg-carbonInputGray text-black text-sm rounded-md w-full p-2.5 dark:placeholder-gray-400 focus:outline-none focus:ring-2 focus:ring-carbonGray"
                  placeholder="Saisir la date de début"
                  id="startDate"
                  name="startDate"
                  value={startDate}
                  onChange={(e) => setStartDate(e.target.value)}
                />
              </div>

              <div>
                <label htmlFor="endDate" className="block mb-2 text-sm font-medium text-carbonGray">Date de fin de formation</label>
                <input
                  type="date"
                  className="bg-carbonInputGray text-black text-sm rounded-md w-full p-2.5 dark:placeholder-gray-400 focus:outline-none focus:ring-2 focus:ring-carbonGray"
                  id="endDate"
                  name="endDate"
                  value={endDate}
                  onChange={(e) => setEndDate(e.target.value)}
                />
              </div>

              <div>
                <label className="block mb-2 text-sm font-medium text-carbonGray" htmlFor="title">Adresse</label>
                <input
                  type="text"
                  className="bg-carbonInputGray text-black text-sm rounded-md w-full p-2.5 dark:placeholder-gray-400 focus:outline-none focus:ring-2 focus:ring-carbonGray"
                  id="adress"
                  name="adress"
                  value={adress}
                  placeholder="Adresse"
                  onChange={(e) => setAdress(e.target.value)}
                  required
                />
              </div>

            </div>
            <button
              type="submit"
              className="text-white bg-carbonBlue hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-xl text-sm w-full sm:w-auto px-5 py-2.5 text-center">
              Créer la formation
            </button>
          </form >
        </div>
      </div>
    </div >
  );
};

export default NewTraining;