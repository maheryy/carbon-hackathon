import { NextPageContext } from "next";
import Link from "next/link";
import { useRouter } from "next/router";
import { getTraining } from "services/training";
import ProfileCard from "components/card/ProfileCard";
import ConsultantSection from "components/dashboard/ConsultantSection";
import { User } from "@prisma/client";
import { serializeProps } from "utils/parser";
import { FaChevronLeft } from "react-icons/fa";
import TrainingHero from "components/dashboard/TrainingHero";
import ConsultantListItem from "components/dashboard/ConsultantListItem";
import { FaEdit, FaTrash } from "react-icons/fa";
import { UserWithBadges } from "types/user";
import { getAllUsers, getUserByEmail } from "services/user";
import { getServerSession } from "next-auth";
import { authOptions } from "pages/api/auth/[...nextauth]";

export const getServerSideProps = async (context) => {
  const session = await getServerSession(context.req, context.res, authOptions);

  if (!session) {
    return {
      redirect: { destination: "/login", permanent: false },
    };
  }
  const id = parseInt(context.query.id as string);
  const training = await getTraining(id);
  const user = await getUserByEmail(session.user.email);
  const consultants = (await getAllUsers()).slice(0, 3);

  return { props: serializeProps({ user, consultants, training }) };
};

const Training = ({ training, user, consultants }: any) => {
  const router = useRouter();

  const onDelete = async () => {
    const res = await fetch(`/api/training/${training.id}`, {
      method: "DELETE",
    });

    if (res.status === 200) {
      router.push("/training");
    } else {
      console.error("Fail to delete : " + res.statusText);
    }
  };

  return (
    <div>
      <div className="pl-8">
        <Link href={`/training`} className="hover:underline flex items-center">
          <FaChevronLeft className="mr-2" />
          Formations
        </Link>
      </div>

      <div className="grid grid-cols-3 gap-12 p-8 rounded">
        <div className="col-span-2 flex flex-col bg-white shadow-xl">
          <div className="relative">
            <TrainingHero />
            <div
              className="absolute flex items-center justify-center"
              style={{
                width: "102px",
                height: "96px",
                right: "calc(50% + 90px)",
                top: "calc(50% + 75px)",
                background: "#FFFFFF",
                boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
                borderRadius: "20px",
              }}
            >
              <span className="text-center text-xl">
                JUIN
                <br />
                25
              </span>
            </div>
          </div>

          <div className="flex">
            <div className="flex flex-col p-8">
              <div className="flex items-center mt-[45px] space-x-4 mb-4">
                <Link
                  href={`/training/${training.id}/edit`}
                  className="hover:underline flex items-center"
                >
                  <FaEdit className="mr-2" /> Modifier
                </Link>
                <button
                  onClick={onDelete}
                  className="hover:underline flex items-center"
                >
                  <FaTrash className="mr-1" /> Supprimer
                </button>
              </div>

              <div className="bg-bgGreen bg-opacity-30 mb-4 rounded-md flex items-center justify-center text-textGreen h-15">
                Demande d'inscription
              </div>
            </div>

            <div className="text-justify p-8">
              <div className="flex flex-col">
                <h1 className="text-xl text-gray-900 mb-4">{training.title}</h1>
                <div className="bg-blue-400 bg-opacity-40 mb-4 rounded-md flex items-center justify-center text-blue-900 w-200 h-100 w-40">
                  25 juin 19:00 - 20:30
                </div>

                <div className="mb-4">
                  <div className="grid grid-cols-2 gap-4">
                    <div className="">
                      Places restantes
                      <div className="text-blue-900 text-sm">15</div>
                    </div>
                    <div className="">
                      Clôture
                      <div className="text-blue-900 text-sm">05 juin 2023</div>
                    </div>
                  </div>
                </div>

                <div className="mb-5">{training.description}</div>

                <h1 className="text-xl text-gray-900 mb-4">
                  Liste des participants
                </h1>
                <div className="space-y-4">
                  {consultants.map((consultant) => (
                    <ConsultantListItem
                      key={consultant.id}
                      consultant={consultant}
                    />
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="col-span-1 flex flex-col gap-6">
          <div>
            <ProfileCard user={user} />
          </div>
          <div>
            <ConsultantSection consultants={consultants} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Training;
