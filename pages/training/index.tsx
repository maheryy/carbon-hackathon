import { Training, User } from "@prisma/client";
import Link from "next/link";
import { getAllTrainings } from "services/training";
import { HiPlus } from "react-icons/hi";
import TrainingCard from "components/card/TrainingCard";
import ProfileCard from "components/card/ProfileCard";
import ConsultantSection from "components/dashboard/ConsultantSection";
import { serializeProps } from "utils/parser";
import { UserWithBadges } from "types/user";
import { getAllUsers, getUserByEmail } from "services/user";
import { getServerSession } from "next-auth";
import { authOptions } from "pages/api/auth/[...nextauth]";

export const getServerSideProps = async (context) => {
  const session = await getServerSession(context.req, context.res, authOptions);

  if (!session) {
    return {
      redirect: { destination: "/login", permanent: false },
    };
  }

  const user = await getUserByEmail(session.user.email);
  const trainings = await getAllTrainings();
  const consultants = (await getAllUsers()).slice(0, 3);

  return { props: serializeProps({ trainings, user, consultants }) };
};

const TrainingList = ({ trainings, user, consultants }: TrainingProps) => {
  return (
    <div className="col-span-2 flex flex-col gap-4">
      <div className="grid grid-cols-3 gap-12">
        <div className="grid grid-cols-1 col-span-2 gap-4">
          <h1 className="text-lg text-gray-900 pt-4">Les formations</h1>

          <Link
            href="/training/new"
            className="flex items-center text-blue-700 hover:underline pb-2"
          >
            <HiPlus className="mr-1 w-4 h-4" /> Créer une nouvelle formation
          </Link>
          <div className="relative">
            <input
              type="text"
              placeholder="Recherche"
              className="pl-12 w-2/5 border h-10 p-4 bg-[#F9F9F9] border-[#E8E8EA] rounded-2xl"
            />
            <span className="absolute left-4 top-1/2 -translate-y-1/2">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-5 h-5"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
                />
              </svg>
            </span>
          </div>
        </div>
        <div className="grid grid-cols-1 md:grid-cols-2 gap-4 col-span-2">
          {trainings.map((training) => (
            <TrainingCard key={training.id} training={training} />
          ))}
        </div>

        <div className="col-span-1 flex flex-col gap-6">
          <div>
            <ProfileCard user={user} />
          </div>
          <div>
            <ConsultantSection consultants={consultants} />
          </div>
        </div>
      </div>
    </div>
  );
};

interface TrainingProps {
  user: UserWithBadges;
  trainings: Training[];
  consultants: User[];
}

export default TrainingList;
