import Card from "components/card";
import Navbar from "components/navbar";
import { getAllUsers } from "services/user";

export const getServerSideProps = async () => {
  const users = await getAllUsers();

  const parseUser = JSON.parse(JSON.stringify(users));

  return { props: { users: parseUser } };
};

const Employee = ({ users }: any) => {
  return (
    <div>
      <div className="flex justify-center">
        <div className="">
          <h1 className="mt-10 mb-10 text-center text-4xl">
            Liste des consultants
          </h1>
          <div className="flex flex-wrap justify-start gap-3 m-5">
            {users.map((user) => (
              <Card key={user.id} user={user} />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Employee;
