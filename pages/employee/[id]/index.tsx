import Card from "components/card";
import ProfileCard from "components/card/ProfileCard";
import Navbar from "components/navbar";
import { NextPageContext } from "next";
import { getAllUsers, getUserById } from "services/user";

export const getServerSideProps = async (context: NextPageContext) => {
    // const users = await getAllUsers();
    const id = parseInt(context.query.id as string);

    const user = await getUserById(id);

    const parseUser = JSON.parse(JSON.stringify(user));

    console.log(parseUser);



    return { props: { users: parseUser } };
};

const Employee = ({ users }: any) => {

    console.log(users);

    return (
        <div className="px-6 grid grid-cols-3 gap-12">
            {/* <Navbar /> */}
            <div className="flex flex-col col-span-2">
                <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
                    <div>
                        <p className="text-lg font-bold">Compétences</p>
                        <div className="bg-white rounded-3xl drop-shadow-lg h-72 mt-4 flex flex-wrap p-[30px] justify-center">
                            {users.skills.map((skill) => (
                                <p className="relative rounded-[10px] bg-[#6996CD] color-white text-white px-8 py-2 font-semibold
                            w-[120px] h-[40px] m-[10px] text-center">{skill}</p>
                            ))}
                        </div>
                    </div>

                    <div>
                        <p className="text-lg font-bold">Badges</p>
                        <div className="bg-white rounded-3xl drop-shadow-lg h-72 mt-4 flex flex-wrap p-[20px]">
                            {users.userBadges.map((data) => (
                                <img src={`/assets/badges/${data.badge.path}`} className="w-[110px] h-auto p-[10px]" />
                            ))}
                        </div>
                    </div>
                </div>


                <div className="w-full flex flex-col">
                    <div>
                        <p className="text-lg font-bold">Clients</p>
                        <div className="bg-white rounded-3xl drop-shadow-lg h-72 w-[100%] mt-10 flex flex-col ">
                            <div className="flex justify-between p-[20px]">
                                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">Entreprise</p>
                                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">Date</p>
                                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">Métier</p>
                            </div>

                            <div className="">
                                {users.missions.map((data) => (
                                    <div className="flex justify-between bg-zinc-100 mt-[10px] p-[10px]">
                                        <div className="flex gap-[10px]">
                                            <img className="mask mask-hexagon w-[50px]" src="https://assets-big.cdn-mousquetaires.com/medias/domain12283/media100002/730-dmwew3stwq.jpg" />
                                            <p className="font-bold text-lg">{data.client.name}</p>
                                        </div>
                                        <p>{data.startDate}</p>
                                        <p className="relative rounded-[10px] bg-[#6996CD] color-white text-white px-8 py-2 font-semibold
                                        w-[100px] h-[40px] text-center">Dev</p>
                                    </div>
                                ))}
                            </div>



                        </div>
                    </div>

                    <div>
                        <p className="mt-4 text-lg font-bold">Formations</p>

                        <div className="bg-white rounded-3xl drop-shadow-lg h-72 mt-10 w-full">
                            <div className="flex justify-between p-[20px]">
                                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">Entreprise</p>
                                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">Date</p>
                                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">Métier</p>
                            </div>

                            {users.userTrainings.map((data) => (
                                <div className="flex justify-between bg-zinc-100 mt-[10px] p-[10px]">
                                    <div className="flex gap-[10px]">
                                        <img className="mask mask-hexagon w-[50px]" src="https://assets-big.cdn-mousquetaires.com/medias/domain12283/media100002/730-dmwew3stwq.jpg" />
                                        <p className="font-bold text-lg">{data.training.title}</p>
                                    </div>
                                    <p>{data.training.startDate}</p>
                                    <p className="relative rounded-[10px] bg-[#6996CD] color-white text-white px-8 py-2 font-semibold
                w-[120px] h-[40px] text-center">{data.status}</p>
                                </div>
                                // <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">{data.training.title}</p>
                            ))}
                        </div>
                    </div>
                </div>

            </div>

            {/* <div className="fpx-8 py-14 flex flex-col items-center gap-4 text-white rounded-[30px] grid grid-cols-3"> */}
            <div className="col-span-1 flex flex-col gap-6">
                <ProfileCard user={users} />
                <div className="bg-carbonBlack-light rounded-3xl drop-shadow-lg h-72">
                    <p className="text-white text-base m-[20px]">Je participe</p>
                    {users.userEvent.map((data) => (
                        <div className="bg-[#6C6E6D] m-auto p-[2px] w-[270px] h-[55px] rounded-[10px] mt-[20px] flex gap-[30px] items-center">
                            <img className="mask mask-hexagon w-[50px] ml-[10px]" src="https://assets-big.cdn-mousquetaires.com/medias/domain12283/media100002/730-dmwew3stwq.jpg" />
                            <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">{data.event.title}</p>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
}

export default Employee;