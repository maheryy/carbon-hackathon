import { NextApiRequest, NextApiResponse } from "next";
import { deleteTraining, getTraining, updateTraining } from "services/training";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const { method, query, body } = req;
  const id = parseInt(query.id as string);

  switch (method) {
    case "GET":
      const training = await getTraining(id);
      res.status(200).json(training);
      break;
    case "PUT":
      const updatedTraining = await updateTraining(id, body);
      res.status(200).json(updatedTraining);
      break;
    case "DELETE":
      const newTraining = await deleteTraining(id);
      res.status(200).json(newTraining);
      break;
    default:
      res.setHeader("Allow", ["GET", "PUT", "DELETE"]);
      res.status(405).end(`Method ${method} Not Allowed`);
  }
};

export default handler;
