import { NextApiRequest, NextApiResponse } from "next";
import { getAllTrainings, createTraining } from "services/training";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const { method, body } = req;

  switch (method) {
    case "GET":
      const trainings = await getAllTrainings();
      res.status(200).json(trainings);
      break;
    case "POST":
      const newTraining = await createTraining(body);
      res.status(201).json(newTraining);
      break;
    default:
      res.setHeader("Allow", ["GET", "POST"]);
      res.status(405).end(`Method ${method} Not Allowed`);
  }
};

export default handler;
