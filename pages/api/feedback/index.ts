import { NextApiRequest, NextApiResponse } from "next";
import { createFeedback, getAllFeedback } from "services/feedback";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const { method, body } = req;

  switch (method) {
    case "GET":
      const feedbacks = await getAllFeedback();
      res.status(200).json(feedbacks);
      break;
    case "POST":
      const newFeedback = await createFeedback(body);
      res.status(201).json(newFeedback);
      break;
    default:
      res.setHeader("Allow", ["GET", "POST"]);
      res.status(405).end(`Method ${method} Not Allowed`);
  }
};

export default handler;
