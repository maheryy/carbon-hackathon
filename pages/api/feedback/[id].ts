import { NextApiRequest, NextApiResponse } from 'next'
import { getFeedback, updateFeedback } from 'services/feedback';

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const { method, query, body } = req;
    const id = parseInt(query.id as string);

    switch (method) {
        case "GET":
          const feedback = await getFeedback(id);
          res.status(200).json(feedback);
          break;
        case "PUT":
          const updatedFeedback = await updateFeedback(id, body);
          res.status(200).json(updatedFeedback);
        default:
          res.setHeader("Allow", ["GET", "PUT", "DELETE"]);
          res.status(405).end(`Method ${method} Not Allowed`);
    }
}

export default handler;