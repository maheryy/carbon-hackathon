const NewsletterHero = () => {
  return (
    <div className="border rounded-[30px] h-[250px] w-full flex items-center justify-center">
      <img
        src="https://s3-alpha-sig.figma.com/img/8469/e1f3/767dbffac9ac4d29ee8a804d276a4718?Expires=1687132800&Signature=ZkkJXWchSV1Om4ZLr5i0SwNZaj9cAtDQQfRN0IQuCX7Y8zQMq7IooTNdW~wlW7SomQwxgUomVKl5yDCgtHRdu4Q0PsCTxgz2C~dhZdvBMnb2~F3xg26jIS~jW0HMZCJoHl54k8dQj1soPLXsgXV-hur9rlMKpRxkX3hE8J59RFwz1xhr8Ublki-nAqDUWFp4I4F25RaSGimXuPA12uXmbLxCAyo42vbBPwrjb4ApXcm4jlbrK1eEazae0FmrlYRoZqRknXODRt-M2XwvwvyJx~2sFOz7s-TSVy2TaaHsAYayoPDMvAqr1Ys15vPTbIt~r1dyGtd3ILp1CqOZjD39sg__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4"
        alt="newsletter-hero"
        className="h-full object-cover w-full rounded-[30px]"
      />
    </div>
  );
};

export default NewsletterHero;
