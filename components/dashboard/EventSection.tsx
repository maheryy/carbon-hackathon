import { Event } from "@prisma/client";
import EventCard from "components/card/EventCard";
import Link from "next/link";

const EventSection = ({ events }: EventSectionProps) => {
  return (
    <section>
      <div className="flex justify-between items-center mb-2">
        <h2 className="font-semibold text-xl">Événements</h2>
        <Link href="#" className="flex items-center gap-1 hover:text-blue-950">
          <span>Voir plus</span>
          <span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-5 h-5"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75"
              />
            </svg>
          </span>
        </Link>
      </div>
      <div className="flex items-center gap-14">
        {events.map((event) => (
          <EventCard key={event.id} event={event} />
        ))}
      </div>
    </section>
  );
};

interface EventSectionProps {
  events: Event[];
}

export default EventSection;
