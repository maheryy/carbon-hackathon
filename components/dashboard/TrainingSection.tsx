import { Training } from "@prisma/client";
import TrainingCard from "components/card/TrainingCard";
import Link from "next/link";

const TrainingSection = ({ trainings }: TrainingSectionProps) => {
  return (
    <section>
      <div className="flex justify-between mb-4">
        <h2 className="font-semibold text-xl">Formations</h2>
        <Link href="/training" className="flex items-center gap-1 hover:text-blue-950">
          <span>Voir plus</span>
          <span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-5 h-5"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75"
              />
            </svg>
          </span>
        </Link>
      </div>
      <div className="flex items-center gap-14">
        {trainings.map((training) => (
          <TrainingCard key={training.id} training={training} />
        ))}
      </div>
    </section>
  );
};

interface TrainingSectionProps {
  trainings: Training[];
}

export default TrainingSection;
