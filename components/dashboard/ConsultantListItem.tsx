import { User } from "@prisma/client";

const ConsultantListItem = ({ consultant }: ConsultantListItemProps) => {
  return (
    <li
      key={consultant.id}
      className="flex items-center p-4 gap-4 bg-[#D9D9D94D] rounded-xl"
    >
      <img
        src={consultant.picture}
        alt={consultant.firstname}
        className="w-10 h-10 rounded-full"
      />
      <span className="capitalize text-lg">
        {consultant.firstname} {consultant.lastname}
      </span>
    </li>
  );
};

interface ConsultantListItemProps {
  consultant: User;
}

export default ConsultantListItem;
