import { User } from "@prisma/client";
import Link from "next/link";
import ConsultantListItem from "./ConsultantListItem";

const ConsultantSection = ({ consultants }: ConsultantSectionProps) => {
  return (
    <div className="bg-carbonBlack-light rounded-[30px] p-8 text-white">
      <div className="flex justify-between items-center mb-4">
        <span className="text-lg">Consultants</span>
        <Link href="#" className="flex items-center gap-1">
          <span>Voir plus</span>
          <span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-5 h-5"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75"
              />
            </svg>
          </span>
        </Link>
      </div>
      <ul className="flex flex-col gap-4">
        {consultants.map((consultant) => (
          <ConsultantListItem key={consultant.id} consultant={consultant} />
        ))}
      </ul>
    </div>
  );
};

interface ConsultantSectionProps {
  consultants: User[];
}

export default ConsultantSection;
