import Badge from "components/Badge";
import { FullUserBadge } from "types/badge";

const lockedBadge = {
  badgeId: 0,
  userId: 0,
  date: new Date("2021-01-01"),
  badge: { path: "bloquer.png", name: "", color: "", id: 0 },
};

const BadgeSection = ({ title, badges }: BadgeSectionProps) => {
  const allBadges = badges.concat(
    new Array(5 - badges.length).fill(lockedBadge)
  );

  return (
    <section>
      <h2 className="text-3xl font-semibold mb-6">{title}</h2>
      <ul className="flex items-center gap-10">
        {allBadges.map((data, key) => (
          <li key={key} className="">
            <Badge path={data.badge.path} />
          </li>
        ))}
      </ul>
    </section>
  );
};

interface BadgeSectionProps {
  title: string;
  badges: FullUserBadge[];
}

export default BadgeSection;
