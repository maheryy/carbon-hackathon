const ConsultantCard = ({user, feedback}) => {
  return (
    <div className="border border-gray-200 rounded-3xl shadow bg-white drop-shadow-lg w-96">
      <div className="p-5">
        <div className="flex items-center">
          <img className="mask mask-hexagon w-32" src={user.picture} />
          <div className="flex flex-col">
            <h5 className="mb-2 text-2xl font-normal tracking-tight text-carbonBlack ml-6">{user.firstname} {user.lastname}</h5>
            <p className="mb-2 font-normal tracking-tight text-carbonRed text-sm ml-6">{user.experience}</p>
          </div>
        </div>
        <div className="flex flex-col justify-evenly mt-6">
          <p className="text-sm">{user.position}</p>
          <p className="text-sm">{user.email}</p>
        </div>
        <div className="relative">
          <a href={`/feedback/${feedback}/detailFeedback`} className="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-colorButton rounded-lg mt-2 absolute bottom-0 right-0">
            Voir le retour
            <svg aria-hidden="true" className="w-4 h-4 ml-2 -mr-1" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z" clipRule="evenodd"></path></svg>
          </a>
        </div>
      </div>
    </div>
  )
}

export default ConsultantCard;