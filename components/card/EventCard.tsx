import { Event } from "@prisma/client";
import Link from "next/link";

const EventCard = ({ event }: EventCardProps) => {
  return (
    <div className="rounded-[30px] shadow-md relative flex-1">
      <Link className="rounded-[30px]" href="#">
        <div className="rounded-[30px] bg-black w-full h-[250px]">
          <img
            src="https://picsum.photos/400"
            alt="formation"
            className="rounded-[30px] w-full h-full object-cover"
          />
        </div>
        <div
          style={{
            clipPath:
              "polygon(100% 100%, 0% 100% , 0.00% 1.31%, 1.33% 1.05%, 2.67% 0.82%, 4.00% 0.61%, 5.33% 0.43%, 6.67% 0.29%, 8.00% 0.17%, 9.33% 0.08%, 10.67% 0.03%, 12.00% 0.00%, 13.33% 0.01%, 14.67% 0.05%, 16.00% 0.12%, 17.33% 0.22%, 18.67% 0.35%, 20.00% 0.52%, 21.33% 0.71%, 22.67% 0.93%, 24.00% 1.17%, 25.33% 1.44%, 26.67% 1.74%, 28.00% 2.05%, 29.33% 2.39%, 30.67% 2.75%, 32.00% 3.12%, 33.33% 3.50%, 34.67% 3.90%, 36.00% 4.31%, 37.33% 4.72%, 38.67% 5.14%, 40.00% 5.56%, 41.33% 5.98%, 42.67% 6.40%, 44.00% 6.82%, 45.33% 7.23%, 46.67% 7.63%, 48.00% 8.01%, 49.33% 8.39%, 50.67% 8.74%, 52.00% 9.08%, 53.33% 9.40%, 54.67% 9.70%, 56.00% 9.98%, 57.33% 10.23%, 58.67% 10.45%, 60.00% 10.65%, 61.33% 10.82%, 62.67% 10.96%, 64.00% 11.06%, 65.33% 11.14%, 66.67% 11.19%, 68.00% 11.20%, 69.33% 11.18%, 70.67% 11.13%, 72.00% 11.05%, 73.33% 10.94%, 74.67% 10.80%, 76.00% 10.62%, 77.33% 10.42%, 78.67% 10.19%, 80.00% 9.94%, 81.33% 9.66%, 82.67% 9.36%, 84.00% 9.04%, 85.33% 8.69%, 86.67% 8.33%, 88.00% 7.96%, 89.33% 7.57%, 90.67% 7.17%, 92.00% 6.76%, 93.33% 6.34%, 94.67% 5.92%, 96.00% 5.50%, 97.33% 5.08%, 98.67% 4.66%, 100.00% 4.25%)",
            background:
              "linear-gradient(0deg, rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), radial-gradient(50% 50% at 50% 50%, rgba(136, 202, 178, 0.2) 0%, rgba(67, 125, 125, 0.138) 100%)",
            backdropFilter: "blur(25px)",
          }}
          className="absolute bg-[#00000033] bottom-0 w-full h-[40%] rounded-b-[30px] text-white px-8 py-7"
        >
          <span className="text-xl font-semibold">{event.title}</span>
          <p className="text-sm">{event.description}</p>
        </div>
      </Link>
    </div>
  );
};

interface EventCardProps {
  event: Event;
}

export default EventCard;
