import { polygonStyle } from "lib/constants";
import { UserWithBadges } from "types/user";

const ProfileCard = ({ user }: ProfileCardProps) => {
  const badgeProgress = user.userBadges.length
    ? Math.round((user.userBadges.length / 12) * 100)
    : 0;

  return (
    <div className="p-6 flex flex-col items-center gap-4 bg-carbonBlack-light text-white rounded-[30px]">
      <div
        style={{ ...polygonStyle }}
        className="bg-slate-300 w-[183px] h-[183px] flex items-center justify-center"
      >
        <img style={{ ...polygonStyle }} width={180} src={user.picture} />
      </div>

      <div className="flex flex-col items-center">
        <h2 className="font-semibold text-4xl">
          {user.firstname} {user.lastname}
        </h2>
        <span>{user.experience}</span>
      </div>
      <hr className="w-3/4 border-[1.5px] my-3" />
      {!!user.userBadges.length && (
        <>
          <div className="flex flex-col gap-3 w-full">
            <div className="flex justify-between">
              <span className="flex items-center gap-3">
                <span className="w-2 h-2 rounded-full bg-[#54B785]"></span>
                <span className="capitalize">
                  {user.experience?.toLowerCase()}
                </span>
              </span>
              <span>{badgeProgress}%</span>
            </div>
            <div className="relative w-full h-3 bg-white rounded-full">
              <div
                style={{ width: `${badgeProgress}%` }}
                className={`absolute h-full bg-[#54B785] rounded-full`}
              ></div>
            </div>
          </div>
          <div className="flex items-center gap-6 mt-4">
            {user.userBadges.slice(0, 3).map(({ badge }, key) => (
              <span key={`badge_${key}`} className="">
                <img
                  src={`/assets/badges/${badge.path}`}
                  alt={`badge_${badge.path.replace("/", "_")}`}
                />
              </span>
            ))}
          </div>
          <hr className="w-3/4 border-[1.5px] my-3" />
        </>
      )}
      {!!user.skills.length && (
        <ul className="flex items-center justify-center flex-wrap gap-6">
          {user.skills.slice(0, 3).map((skill, key) => (
            <li
              key={`skills_${key}`}
              className="relative rounded-[46px] bg-[#6996CD] color-white px-8 py-2 font-semibold"
            >
              <span>{skill}</span>
              <img
                className="absolute right-0 -top-1"
                src="/assets/icons/heart.svg"
                alt="preferred"
                width={18}
                height={18}
              />
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

interface ProfileCardProps {
  user: UserWithBadges;
}

export default ProfileCard;
