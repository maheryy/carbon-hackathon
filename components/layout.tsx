import { useSession } from "next-auth/react";
import Navbar from "./navbar";
import { notFound, redirect, useRouter } from "next/navigation";

export default function Layout({ children }) {
  const { data, status, update } = useSession();
  const router = useRouter();

  if (status === "loading") {
    return <div>Loading...</div>;
  }

  if (status === "unauthenticated") {
    router.replace("/login");
    return;
  }

  return (
    <>
      <Navbar />
      <main className="md:ml-64 p-5">{children}</main>
    </>
  );
}
