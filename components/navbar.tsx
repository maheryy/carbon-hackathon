import Link from "next/link";
import { VscFeedback } from "react-icons/vsc";

const Navbar = () => {
  return (
    <>
      <div className="flex flex-col md:flex-row md:items-center">
        <aside
          id="default-sidebar"
          className="fixed top-0 left-0 z-40 w-64 h-screen transition-transform -translate-x-full sm:translate-x-0"
          aria-label="Sidebar"
        >
          <div className="h-full px-3 py-4 overflow-y-auto bg-carbonBlack">
            <img src="assets/Logo.svg" className="mb-10" />
            <hr className="h-px my-8 bg-gray-200 border-0 dark:bg-gray-700 mb-8"></hr>
            <ul className="space-y-2 font-medium">
              <li>
                <Link
                  href="/"
                  className="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-black"
                >
                  <img src="/assets/Navbar/Explore.svg" />
                  <span className="ml-3">Accueil</span>
                </Link>
              </li>
              <li>
                <Link
                  href="#"
                  className="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-black"
                >
                  <img src="/assets/Navbar/Music.svg" />
                  <span className="flex-1 ml-3 whitespace-nowrap">
                    Événements
                  </span>
                </Link>
              </li>
              <li>
                <Link
                  href="/training"
                  className="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-black"
                >
                  <img src="/assets/Navbar/Game.svg" />
                  <span className="flex-1 ml-3 whitespace-nowrap">
                    Formations
                  </span>
                </Link>
              </li>
              <li>
                <Link
                  href="/badges"
                  className="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-black"
                >
                  <img src="/assets/Navbar/Education.svg" />
                  <span className="flex-1 ml-3 whitespace-nowrap">Badges</span>
                </Link>
              </li>
              <li>
                <Link
                  href="#"
                  className="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-black"
                >
                  <img src="/assets/Navbar/profil.svg" />
                  <span className="flex-1 ml-3 whitespace-nowrap">Profil</span>
                </Link>
              </li>
              <li>
                <Link
                  href="/employee"
                  className="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-black"
                >
                  <img src="/assets/Navbar/trombinoscope.svg" />
                  <span className="flex-1 ml-3 whitespace-nowrap">
                    Trombinoscope
                  </span>
                </Link>
              </li>
              <li>
                <Link
                  href="/feedback"
                  className="flex items-center p-2 text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-black"
                >
                  <VscFeedback size={18} />
                  <span className="flex-1 ml-3 whitespace-nowrap">
                    Retours clients
                  </span>
                </Link>
              </li>
            </ul>
          </div>
        </aside>
      </div>
    </>
  );
};

export default Navbar;
