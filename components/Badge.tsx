const Badge = ({ path }: BadgeProps) => {
  return (
    <span>
      <img
        src={`/assets/badges/${path.replace(/^\//, "")}`}
        alt={`badge_${path.replace("/", "_")}`}
      />
    </span>
  );
};

interface BadgeProps {
  path: string;
}

export default Badge;
