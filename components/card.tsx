const Card = ({user}) => {
    return (
        <a href={`/employee/${user.id}`}>
            <div className="max-w-sm border border-gray-200 rounded-3xl shadow bg-white group/item hover:bg-slate-100 drop-shadow-lg w-96">
                <div className="p-5">
                    <div className="flex items-center">
                        <img className="mask mask-hexagon w-32" src={user.picture} />
                        <div className="flex flex-col">
                            <h5 className="mb-2 text-2xl font-normal tracking-tight text-carbonBlack ml-6">{user.firstname} {user.lastname}</h5>
                            <p className="mb-2 font-normal tracking-tight text-carbonRed text-sm ml-6">{user.experience}</p>
                        </div>
                    </div>
                    <div className="flex flex-col justify-evenly mt-6">
                        <p className="text-sm">{user.position}</p>
                        <p className="text-sm">{user.email}</p>
                    </div>
                    <div className="flex justify-evenly mt-6">
                        {user.skills.map((skill, indexSkill) => (
                            <p key={indexSkill} className="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-colorButton rounded-lg mt-6">{skill}</p>
                        ))}
                    </div>
                </div>
            </div>
        </a>
    )
}

export default Card;