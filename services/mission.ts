import { Feedback, Mission } from "@prisma/client";
import prisma from "lib/prisma";

export const getAllClientsConsultant = async (id: number) => {
    const clientsConsultant = await prisma.mission.findMany({
      where: {
        clientId: id
      },
      include: {
        consultant: true,
        client: true,
      },
    });
  
    const formattedClientsConsultant = clientsConsultant.map((clientConsultant) => ({
      ...clientConsultant,
      startDate: clientConsultant.startDate.toISOString(),
      consultant: {
        ...clientConsultant.consultant,
        contractStart: clientConsultant.consultant.contractStart.toISOString(),
      },
      client: {
        ...clientConsultant.client,
      }
    }))
  
    return formattedClientsConsultant;
  }