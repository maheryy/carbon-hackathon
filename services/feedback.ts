import { Feedback, User } from "@prisma/client";
import prisma from "lib/prisma";

type FeedbackWithoutId = Omit<Feedback, "id">;

export const createFeedback = async (data: FeedbackWithoutId) => {
  return prisma.feedback.create({
    data: { ...data },
  });
};

export const getAllFeedback = async () => {
  const feedbacks =  await prisma.feedback.findMany(
    {
      include: {
        consultant: true,
      },
    }
  );
  const formattedFeedbacks = feedbacks.map((feedback) => ({
    ...feedback,
    date: feedback.date.toISOString(),
    consultant: {
      ...feedback.consultant,
      contractStart: feedback.consultant.contractStart.toISOString(),
      birthday: feedback.consultant.birthday.toISOString(),
    }
  }))

  return formattedFeedbacks;
}

export const getAllFeedbackByClient = async (id: number) => {
  const feedbackByclient = await prisma.feedback.findMany({
    where: {
      clientId: id
    },
    include: {
      consultant: true,
      client: true
    },
  });

  const formattedFeedbackByClient = feedbackByclient.map((feedback) => ({
    ...feedback,
    date: feedback.date.toISOString(),
    consultant: {
      ...feedback.consultant,
      contractStart: feedback.consultant.contractStart.toISOString(),
      birthday: feedback.consultant.birthday.toISOString(),
    }
  }))

  return formattedFeedbackByClient;
}

export const updateFeedback = async (id: number, data: FeedbackWithoutId) => {
  return prisma.feedback.update({
    where: { id },
    data: { ...data },
  });
};

export const getFeedback = async (id: number) => {
  const feebackId = await prisma.feedback.findUnique({
    where: { id },
    include: {
      consultant: true,
      client: true
    },
  });

  const feedback = {
    ...feebackId,
    date: feebackId.date.toISOString(),
    consultant: {
      ...feebackId.consultant,
      contractStart: feebackId.consultant.contractStart.toISOString(),
      birthday: feebackId.consultant.birthday.toISOString(),
    }
  }

  return feedback;

};