import { Feedback, Mission } from "@prisma/client";
import prisma from "lib/prisma";

export const getAllEvents = async () => {
  return prisma.event.findMany();
};
