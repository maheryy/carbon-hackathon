import prisma from "lib/prisma";

export const getAllUsers = async () => {
  const users = prisma.user.findMany();
  return users;
};

export const getUserWithBadges = async (id: number) => {
  return prisma.user.findUnique({
    include: { userBadges: { include: { badge: true } } },
    where: { id },
  });
};



export const getUserById = async (id: number) => {
  const result = await prisma.user.findUnique({
    where: { id },
    include: {
      userTrainings: {
        include: {
          training: true,
        },
      },
      userEvent: {
        include: {
          event: true,
        },
      },
      userBadges: {
        include: {
          badge: true,
        },
      },
      missions: {
        include: {
          client: true,
        },
      },
    },
  });

  const user = {
    ...result,
    birthday: result.birthday.toISOString(),
    // contractEnd: result.contractEnd.toISOString(),
    contractStart: result.contractStart.toISOString(),
  };

  return user;
};

export const getUserByEmail = async (email: string) => {
  const result = await prisma.user.findUnique({
    where: { email },
    include: {
      userTrainings: {
        include: {
          training: true,
        },
      },
      userEvent: {
        include: {
          event: true,
        },
      },
      userBadges: {
        include: {
          badge: true,
        },
      },
      missions: {
        include: {
          client: true,
        },
      },
    },
  });

  const user = {
    ...result,
    birthday: result.birthday.toISOString(),
    contractEnd: result.contractEnd.toISOString(),
    contractStart: result.contractStart.toISOString(),
  };

  return user;
};

export const getUser = async (id: number) => {
  const user = await prisma.user.findUnique({
    where: { id },
  });

  return JSON.parse(JSON.stringify(user));
};

export const getSimpleUserByEmail = async (email: string) => {
  return prisma.user.findUnique({ where: { email } });
};
