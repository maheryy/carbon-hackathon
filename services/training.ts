import { Training } from "@prisma/client";
import prisma from "lib/prisma";
import { format } from 'date-fns';

type TrainingWithoutId = Omit<Training, "id">;

export const createTraining = async (data: any) => {
    return prisma.training.create({
        data: {
            ...data
        },
    });
};

export const updateTraining = async (id: number, data: TrainingWithoutId) => {
    return prisma.training.update({
        where: { id },
        data: { ...data },
    });
};

export const deleteTraining = async (id: number) => {
    return prisma.training.delete({
        where: { id },
    });
};

export const getAllTrainings = async () => {
    const trainings = await prisma.training.findMany();
    // Convertir les dates en chaînes de caractères
    const formattedTrainings = trainings.map((training) => ({
        ...training,
        startDate:  format(new Date(training.startDate), 'dd/MM/yyyy'),
        endDate: format(new Date(training.endDate), 'dd/MM/yyyy')
    }));

    return formattedTrainings;
};

export const getTraining = async (id: number) => {

    const training = await prisma.training.findUnique({
        where: { id },
    });

    const formattedTraining = {
        ...training,
        startDate: training.startDate.toISOString(),
        endDate: training.endDate.toISOString()
    };
    return formattedTraining;
};