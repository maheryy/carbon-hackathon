export const serializeProps = (props: Record<string, any>) =>
  JSON.parse(JSON.stringify(props));
