/** @type {import('tailwindcss').Config} */
module.exports = {
  daisyui: {
    themes: ["light"],
  },
  content: ["./components/**/*.tsx", "./pages/**/*.tsx"],
  theme: {
    extend: {
      colors: {
        'accent-1': '#FAFAFA',
        'accent-2': '#EAEAEA',
        'accent-7': '#333',
        success: '#0070f3',
        cyan: '#79FFE1',
        carbonBlack: "#282B2A",
        "carbonBlack-light": "#282b2ae6",
        'inputColor': '#F4F4F4',
        'inputPlaceholderColor': '#7F7E83',
        'inputLabelColor': '#7F7E83',
        'colorButton': '#6996CD',
        'carbonRed': '#E53F49',
        'carbonGreen': '#00BB7E',
        'carbonGray': '#7F7E83',
        'carbonInputGray': '#F4F4F4',
        'carbonBlue': '#6996CD',
        'textGreen': '#00BB7E',
        'bgGreen': '#00BB7E4D'
      },
      spacing: {
        28: "7rem",
      },
      letterSpacing: {
        tighter: "-.04em",
      },
      lineHeight: {
        tight: 1.2,
      },
      fontSize: {
        "5xl": "2.5rem",
        "6xl": "2.75rem",
        "7xl": "4.5rem",
        "8xl": "6.25rem",
      },
      boxShadow: {
        sm: "0 5px 10px rgba(0, 0, 0, 0.12)",
        md: "0 8px 30px rgba(0, 0, 0, 0.12)",
      },
    },
  },
  plugins: [require("daisyui")],
}
